Here's the idea:

1. Get bunch of C# devs: job mates, local community, friends, or even online. 
2. Grab the single .cs file from repository 
3. Try solve each exercise (make test pass) with single Linq query in short iterations (10 min). 
4. Discuss your solution with mates after each iteration 

You can go with: 
- codding dojo (single comp + beamer) 
- pair programming 
- individuals working 

Enjoy! 
